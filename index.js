"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/ban-ts-ignore */
const querystring_1 = require("querystring");
const random_1 = __importDefault(require("@dl2/utils/string/random"));
const exception_1 = require("@dl2/utils/exception");
const objection_1 = require("objection");
const constants_1 = require("./constants");
/**
 * Complementary class
 * for [`objection.js`](https://vincit.github.io/objection.js).
 */
class Model extends objection_1.Model {
    static delete(filter) {
        // @ts-ignore
        return this.query()
            .returning("*")
            .patch({
            deletedAt: objection_1.raw(constants_1.CURRENT_TIMESTAMP),
            lockoutReason: "scheduled for deletion",
            recoveryToken: random_1.default(64),
        })
            .where({ ...filter })
            .whereNull("deletedAt");
    }
    static restore(recoveryToken) {
        // @ts-ignore
        return this.query()
            .returning("*")
            .patch({
            deletedAt: null,
            lockoutReason: null,
            recoveryToken: null,
        })
            .where({ recoveryToken })
            .whereNotNull("deletedAt");
    }
    /**
     * @todo(douggr): add JSDoc
     */
    static $findQuery(query) {
        if (query) {
            if (typeof query === "string") {
                query = { $page: {}, ...querystring_1.parse(query) };
            }
            if (!query.$page) {
                query.$page = {
                    number: "1",
                    size: constants_1.DEFAULT_PAGESIZE,
                };
            }
            if ("$page[number]" in query) {
                query.$page.number = query["$page[number]"];
                delete query["$page[number]"];
            }
            if ("$page.number" in query) {
                query.$page.number = query["$page.number"];
                delete query["$page.number"];
            }
            if ("$page[size]" in query) {
                query.$page.size = query["$page[size]"];
                delete query["$page[size]"];
            }
            if ("$page.size" in query) {
                query.$page.size = query["$page.size"];
                delete query["$page.size"];
            }
        }
        const { $page = { number: "1", size: constants_1.DEFAULT_PAGESIZE }, $sort, ...rest } = query || {};
        //
        // [BEGIN pagination]
        $page.number = parseInt(($page.number || 1), 10);
        $page.size = parseInt(($page.size || constants_1.DEFAULT_PAGESIZE), 10);
        if ($page.number < 1) {
            $page.number = 1;
        }
        if ($page.size < 1) {
            $page.size = 1;
        }
        if ($page.size > constants_1.MAX_PAGESIZE) {
            $page.size = constants_1.MAX_PAGESIZE;
        }
        // [END pagination]
        // @ts-ignore #2684
        const queryBuilder = this.query()
            .skipUndefined()
            .limit($page.size)
            .offset(--$page.number * $page.size);
        const searchable = this.searchableColumns;
        if (!rest || !searchable || !Object.keys(searchable).length) {
            return queryBuilder;
        }
        return queryBuilder;
    }
    /**
     * Exports this model as a JSON object.
     */
    toJSON(opt) {
        const json = super.toJSON(opt);
        Object.keys(json).forEach((column) => {
            if (this.$hidden().includes(column)) {
                delete json[column];
            }
        });
        return json;
    }
    /**
     * Called before a model is deleted.
     */
    async $beforeDelete(qc) {
        // @ts-ignore
        const { columns } = this.tableMetadata();
        if (columns.includes("deleted_at")) {
            throw new exception_1.Exception({
                message: "unable to delete; use 'Model.delete(filter)' instead.",
                type: "invalid",
            });
        }
        return await Promise.resolve(() => void 0);
    }
    /**
     * Called before a model is inserted into the database.
     */
    async $beforeInsert(qc) {
        return Promise.resolve(this.$beforeSave(qc));
    }
    /**
     * Called before a model is updated.
     */
    async $beforeUpdate(opt, qc) {
        return this.$beforeSave(qc).then(() => {
            // @ts-ignore
            const { columns } = this.tableMetadata();
            if (columns.includes("updated_at")) {
                // @ts-ignore
                this.updatedAt = objection_1.raw(constants_1.CURRENT_TIMESTAMP);
            }
        });
    }
    /**
     * Called as a hook before `$beforeInsert` and `$beforeUpdate`.
     */
    async $beforeSave(qc) {
        // does nothing by default.
        return await Promise.resolve(() => void 0);
    }
    /**
     * Sometimes you may wish to limit the attributes, such as
     * passwords, that are included in your model's array or JSON
     * representation.
     *
     * To do so, return the attributes you want to remove from the
     * public access.
     */
    $hidden() {
        return [];
    }
}
exports.default = Model;
//# sourceMappingURL=index.js.map