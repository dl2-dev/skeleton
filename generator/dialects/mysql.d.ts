export declare function gettype(type: string): string;
export declare function describe(table: string, schema?: string): AsyncGenerator<{
    enum: string | null;
    maxLength: number;
    name: any;
    nullable: boolean;
    primary: boolean;
    type: any;
    typedef: string;
}, void, unknown>;
