export declare function describe(table: string, schema?: string): AsyncGenerator<{
    maxLength: number;
    name: any;
    nullable: boolean;
    primary: boolean;
    type: any;
    typedef: string;
}, void, unknown>;
