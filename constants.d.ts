export { CURRENT_TIMESTAMP, DEFAULT_PAGESIZE, MAX_PAGESIZE, SEARCH_OPERATORS };
/**
 * Raw value for `CURRENT_TIMESTAMP` in sql queries.
 */
declare const CURRENT_TIMESTAMP: any;
/**
 * Default page size while fetching data.
 */
declare const DEFAULT_PAGESIZE = "100";
/**
 * Max page size while fetching data.
 */
declare const MAX_PAGESIZE = 1000;
/**
 * Basic search operators used by supported models:
 *
 *  - `bw`: begins with
 *  - `eq`: equals
 *  - `ew`: ends with
 *  - `gt`: greater than, inclusive
 *  - `has`: has partial
 *  - `in`: in `string[]`, uses "eq" if only 1 item
 *  - `like`: has partial
 *  - `lt`: lesser than, inclusive
 *  - `neq`: not equals
 *  - `not`: not in `string[]`, uses "neq" if only 1 item
 */
declare const SEARCH_OPERATORS: {
    bw: string;
    eq: string;
    ew: string;
    gt: string;
    has: string;
    in: string;
    like: string;
    lt: string;
    neq: string;
    not: string;
};
