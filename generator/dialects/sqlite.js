"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("../../knex"));
const mysql_1 = require("./mysql");
async function* describe(table, schema) {
    if (schema) {
        table = [schema, table].join("`.`");
    }
    const rowset = await knex_1.default().raw(`PRAGMA TABLE_INFO(\`${table}\`)`);
    for (const { name, notnull, pk, type } of rowset) {
        yield {
            maxLength: +type.replace(/.+\((\d+)\)/gi, "$1") || -1,
            name,
            nullable: !notnull,
            primary: !!pk,
            type,
            typedef: mysql_1.gettype(type),
        };
    }
}
exports.describe = describe;
//# sourceMappingURL=sqlite.js.map