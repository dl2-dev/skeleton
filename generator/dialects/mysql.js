"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("../../knex"));
function gettype(type) {
    if (/^(bool)+/i.test(type)) {
        return "boolean";
    }
    if (/^(decimal|float|int|numeric|real|tinyint)+/i.test(type)) {
        if (type === "tinyint(1)") {
            return "boolean | number";
        }
        return "number";
    }
    if (/^(char|text|uuid|varchar)+/i.test(type)) {
        return "string";
    }
    if (/^(date|time)+/i.test(type)) {
        return "Date | Raw | string";
    }
    return `any /* unknown type '${type}' */`;
}
exports.gettype = gettype;
async function* describe(table, schema) {
    if (schema) {
        table = [schema, table].join("`.`");
    }
    const [rowset] = await knex_1.default().raw(`DESCRIBE \`${table}\``);
    for (const { Field, Null, Key, Type } of rowset) {
        let enumType = null;
        if (Type.substr(0, 5) === "enum(") {
            enumType = Type.replace(/enum\((.+)\)/gi, "$1")
                .replace("','", '","')
                .replace(/'/g, "");
        }
        yield {
            enum: enumType,
            maxLength: +Type.replace(/.+\((\d+)\)/gi, "$1") || -1,
            name: Field,
            nullable: Null === "YES",
            primary: Key.toUpperCase() === "PRI",
            type: Type,
            typedef: gettype(Type),
        };
    }
}
exports.describe = describe;
//# sourceMappingURL=mysql.js.map