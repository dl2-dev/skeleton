# Changelog

## [Unreleased]

Not yet released; provisionally v1.0.0 (may change).

### Added

### Changed

### Removed

[unreleased]: https://bitbucket.org/dl2-dev/node-model-src/branches/compare/HEAD..1.0.0#diff
