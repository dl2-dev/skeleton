import { Model as ObjectionModel, ModelOptions, QueryContext, ToJsonOptions, Raw } from "objection";
import { SEARCH_OPERATORS } from "./constants";
export interface SearchableColumn {
    [searchable: string]: Partial<keyof typeof SEARCH_OPERATORS> | Partial<keyof typeof SEARCH_OPERATORS>[] | "any";
}
export interface FindQueryInterface {
    [searchable: string]: any;
    $page: {
        number: number | string;
        size: number | string;
    };
    $sort: string;
}
/**
 * Complementary class
 * for [`objection.js`](https://vincit.github.io/objection.js).
 */
export default abstract class Model extends ObjectionModel {
    readonly createdAt: Date;
    readonly deletedAt?: Date | Raw;
    readonly updatedAt?: Date | Raw;
    /**
     * Columns allowed to auto-configure within `$findQuery`
     * and `$parseSearchQuery`.
     */
    protected static readonly searchableColumns?: SearchableColumn;
    static delete(filter: any): any;
    static restore(recoveryToken: string): any;
    /**
     * @todo(douggr): add JSDoc
     */
    protected static $findQuery<M extends Model>(query?: string | any): any;
    /**
     * Exports this model as a JSON object.
     */
    toJSON(opt?: ToJsonOptions): import("objection").Pojo;
    /**
     * Called before a model is deleted.
     */
    $beforeDelete(qc: QueryContext): Promise<() => undefined>;
    /**
     * Called before a model is inserted into the database.
     */
    $beforeInsert(qc: QueryContext): Promise<any>;
    /**
     * Called before a model is updated.
     */
    $beforeUpdate(opt: ModelOptions, qc: QueryContext): Promise<void>;
    /**
     * Called as a hook before `$beforeInsert` and `$beforeUpdate`.
     */
    $beforeSave(qc: QueryContext): Promise<any | void>;
    /**
     * Sometimes you may wish to limit the attributes, such as
     * passwords, that are included in your model's array or JSON
     * representation.
     *
     * To do so, return the attributes you want to remove from the
     * public access.
     */
    protected $hidden(): any[];
}
