"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const to_camel_case_1 = __importDefault(require("@dl2/utils/string/to-camel-case"));
exports.toCamelCase = to_camel_case_1.default;
const constants_1 = require("./constants");
exports.SEARCH_OPERATORS = constants_1.SEARCH_OPERATORS;
function $extract(search, separator = "|") {
    const [column, operator = "eq"] = search.split(":");
    const columns = [...new Set(column.split(separator))].map((c) => {
        return to_camel_case_1.default(c);
    });
    return { columns, operator };
}
exports.$extract = $extract;
function $nullSearch(builder, operator, column) {
    const isArray = Array.isArray(column);
    if (operator === constants_1.SEARCH_OPERATORS.eq) {
        if (isArray) {
            return column.forEach((c) => builder.orWhereNull(c));
        }
        return builder.whereNull(column);
    }
    if ([constants_1.SEARCH_OPERATORS.not, constants_1.SEARCH_OPERATORS.neq].includes(operator)) {
        if (isArray) {
            return column.forEach((c) => builder.orWhereNotNull(c));
        }
        return builder.whereNotNull(column);
    }
    return builder;
}
exports.$nullSearch = $nullSearch;
function $searchable(column, operator, searchable) {
    const op = searchable[column];
    if (!op) {
        return false;
    }
    if (operator === null) {
        return op;
    }
    return op === "all" || op.includes(operator);
}
exports.$searchable = $searchable;
//# sourceMappingURL=helpers.js.map