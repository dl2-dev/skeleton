"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Knex = require("knex");
const fs_1 = require("fs");
const path_1 = require("path");
const url_1 = require("url");
const identifierMapping_1 = require("objection/lib/utils/identifierMapping");
const knexConfig = {};
const knexFile = path_1.join(process.cwd(), "knex.js");
// istanbul ignore next
if (fs_1.existsSync(knexFile)) {
    // tslint:disable-next-line: no-var-requires
    Object.assign(knexConfig, require(knexFile));
}
let client = process.env.DATABASE_DRIVER || "postgres";
/**
 * @var typeof Knex.Config.connection
 */
const connection = {
    database: process.env.DATABASE_NAME,
    filename: process.env.DATABASE_NAME,
    host: process.env.DATABASE_HOSTNAME,
    password: process.env.DATABASE_PASSWORD,
    requestTimeout: process.env.DATABASE_TIMEOUT,
    user: process.env.DATABASE_USERNAME,
};
if (!!process.env.DATABASE_URL) {
    const { auth, hostname: host, path: database, port, protocol } = url_1.parse(process.env.DATABASE_URL);
    const [user, password] = (auth || "").split(":");
    Object.assign(connection, {
        database: database === null || database === void 0 ? void 0 : database.replace("/", ""),
        filename: database,
        host,
        password,
        port,
        user,
    });
    client = protocol === null || protocol === void 0 ? void 0 : protocol.replace(/[^a-z0-9]/gi, "");
}
// istanbul ignore next
exports.default = (config = knexConfig) => {
    return Knex({
        client,
        connection: {
            // node 12+ use to reject all self-signed certificates
            ssl: {
                rejectUnauthorized: false,
            },
            ...connection,
        },
        useNullAsDefault: true,
        // overrides
        ...config,
        // these are optionated
        ...identifierMapping_1.knexSnakeCaseMappers(),
    });
};
//# sourceMappingURL=knex.js.map