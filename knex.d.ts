import Knex = require("knex");
declare const _default: (config?: Knex.Config<any>) => Knex<any, unknown[]>;
export default _default;
