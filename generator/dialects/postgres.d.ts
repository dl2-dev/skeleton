export declare function gettype(type: string): string;
export declare function describe(table: string, schema?: string): AsyncGenerator<any, void, unknown>;
