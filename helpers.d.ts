import toCamelCase from "@dl2/utils/string/to-camel-case";
import { SEARCH_OPERATORS } from "./constants";
export { $extract, $nullSearch, $searchable, toCamelCase, SEARCH_OPERATORS, };
declare function $extract(search: string, separator?: string): {
    columns: string[];
    operator: string;
};
declare function $nullSearch(builder: any, operator: string, column: any): any;
declare function $searchable(column: string, operator: string | null, searchable: any): any;
