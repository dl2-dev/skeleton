"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const to_camel_case_1 = __importDefault(require("@dl2/utils/string/to-camel-case"));
const to_snake_case_1 = __importDefault(require("@dl2/utils/string/to-snake-case"));
const handlebars_1 = __importDefault(require("handlebars"));
const knex_1 = __importDefault(require("../knex"));
const HIDDENS = [/password$/, /token$/, /^lockout/];
const READONLY = ["id"];
const SKIPPED = ["createdAt", "deletedAt", "updatedAt"];
const VIRTUALS = {
    deletedAt: "deleted",
    lockoutReason: "suspended",
    verifiedAt: "verified",
};
const $knex = knex_1.default();
const development = path_1.basename(__filename) === "index.ts";
const dialect = $knex.client.config.client;
const { describe } = require(`./dialects/${dialect}`);
(async () => {
    // const $testTable = "test_table";
    // await $knex.schema.dropTableIfExists($testTable);
    // await $knex.schema.createTable($testTable, (table: any) => {
    //   /* date_field       */ table.date("date_field");
    //   /* decimal_field    */ table.decimal("decimal_field");
    //   /* enum_field       */ table.enu("enum_field", ["value1", "value2"]);
    //   /* float_field      */ table.float("float_field");
    //   /* id               */ table.increments("id").primary();
    //   /* is_boolean_field */ table.boolean("is_boolean_field");
    //   /* json_field       */ table.jsonb("json_field");
    //   /* password         */ table.string("password");
    //   /* test_field       */ table.text("test_field");
    //   /* time_field       */ table.time("time_field");
    //   /* username         */ table.string("username");
    //   /* uuid_field       */ table.uuid("uuid_field");
    //   table.timestamps();
    // });
    for (let argc = 2; argc < process.argv.length; argc++) {
        try {
            await generate(process.argv[argc]);
        }
        catch (e) {
            // eslint-disable-next-line no-console
            console.error(e);
            process.exit(1);
        }
    }
    // await $knex.schema.dropTableIfExists($testTable);
    process.exit(0);
})();
async function generate(tableName) {
    const $attrs = [];
    const attributes = [];
    const enumAttributes = [];
    const jsonAttributes = [];
    const primary = [];
    const tableSchema = tableName.split(".");
    const virtuals = [];
    let [schema, table] = tableSchema;
    if (tableSchema.length !== 2) {
        table = schema;
        schema = "";
    }
    const filename = table
        .replace("__", "/")
        .split("/")
        .map((part) => to_snake_case_1.default(part, "-"))
        .join("/");
    const classname = to_camel_case_1.default(path_1.basename(filename), true);
    for await (const metadata of describe(table, schema)) {
        const name = to_camel_case_1.default(metadata.name);
        $attrs.push(name);
        Object.keys(VIRTUALS).forEach((attribute) => {
            if (attribute === name) {
                virtuals.push({ attribute, fn: VIRTUALS[attribute] });
            }
        });
        if (SKIPPED.includes(name)) {
            continue;
        }
        const attr = {
            column: metadata.name,
            enum: metadata.enum,
            hidden: HIDDENS.map(testString(name)).includes(true),
            name,
            nullable: metadata.nullable,
            readonly: READONLY.map(testString(name)).includes(true),
            type: metadata.type,
            typedef: metadata.typedef,
        };
        if (!!metadata.primary) {
            primary.push(name);
        }
        if (/^json+/i.test(metadata.type)) {
            jsonAttributes.push({ ...metadata, name });
        }
        if (!!metadata.enum) {
            enumAttributes.push({ ...metadata, name });
        }
        attributes.push(attr);
    }
    const locals = {
        $attrs,
        attributes,
        classname,
        development,
        enumAttributes,
        filename,
        imports: path_1.basename(filename),
        jsonAttributes,
        primary,
        tableName,
        toCamelCase: to_camel_case_1.default,
        virtuals,
    };
    // prettier-ignore
    {
        writeModelFile("model-base", { ...locals, filename: `${filename}-base` }, true);
        writeModelFile("model-spec", { ...locals, filename: `${filename}.spec` }, development);
        writeModelFile("model", locals, development);
    }
}
// [BEGIN helpers]
handlebars_1.default.registerHelper("json", (input) => {
    return JSON.stringify(input);
});
handlebars_1.default.registerHelper("toCamelCase", (input) => {
    return to_camel_case_1.default(input, true);
});
handlebars_1.default.registerHelper("inArray", (needle, haystack) => {
    if (Array.isArray(haystack)) {
        return haystack.includes(needle);
    }
    return !!haystack[needle];
});
const testString = (name) => {
    return (e) => {
        if (typeof e === "string") {
            return name === e;
        }
        return e.test(name);
    };
};
const writeModelFile = (filename, locals, overwrite = false) => {
    const template = fs_1.readFileSync(`${__dirname}/templates/${filename}.ts.hbs`, "utf8");
    const content = handlebars_1.default.compile(template)(locals);
    const output = path_1.resolve(process.cwd(), `src/models/${locals.filename}.ts`);
    const pathname = path_1.dirname(output);
    if (!fs_1.existsSync(pathname)) {
        fs_1.mkdirSync(pathname, { recursive: true });
    }
    if (!overwrite && fs_1.existsSync(output)) {
        return;
    }
    fs_1.writeFileSync(output, content);
};
// [END helpers]
//# sourceMappingURL=index.js.map