"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("../../knex"));
function gettype(type) {
    if (/^(bool)+/i.test(type)) {
        return "boolean";
    }
    if (/^(decimal|float|int|numeric|real)+/i.test(type)) {
        return "number";
    }
    if (/^(char|text|uuid|varchar)+/i.test(type)) {
        return "string";
    }
    if (/^(date|time)+/i.test(type)) {
        return "Date | Raw | string";
    }
    return `any /* unknown type '${type}' */`;
}
exports.gettype = gettype;
async function* describe(table, schema) {
    const { rows: rowset } = await knex_1.default().raw(`
    SELECT * FROM (
      SELECT
        a.attlen AS "maxLength",
        a.attname AS "name",
        a.attnotnull = TRUE AS "nullable",
        co.contype = 'p' as "primary",
        FORMAT_TYPE(a.atttypid, a.atttypmod) AS "type"
      FROM pg_attribute AS a
        JOIN pg_class AS c ON a.attrelid = c.oid
        LEFT OUTER JOIN
          pg_constraint AS co
            ON (co.conrelid = c.oid AND a.attnum = ANY(co.conkey) AND co.contype = 'p')
      WHERE TRUE
        AND a.attnum > 0
        AND a.attrelid = '"${schema || "public"}"."${table}"'::regclass
    ) dd
    LEFT JOIN (
        SELECT
          STRING_AGG(e.enumlabel, '","') "enum",
          t.typname
        FROM
          pg_enum e
        INNER JOIN pg_type t
          ON e.enumtypid = t.oid
        GROUP BY t.typname
      ) ff
      ON ff.typname = dd.type
    ORDER BY
      "name"`);
    for await (const { type, ...row } of rowset) {
        yield { ...row, type, typedef: gettype(type) };
    }
}
exports.describe = describe;
//# sourceMappingURL=postgres.js.map