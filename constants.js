"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    // istanbul ignore next
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("./knex"));
/**
 * Raw value for `CURRENT_TIMESTAMP` in sql queries.
 */
const CURRENT_TIMESTAMP = require(`./dialects/${knex_1.default().client.config.client}`).default;
exports.CURRENT_TIMESTAMP = CURRENT_TIMESTAMP;
/**
 * Default page size while fetching data.
 */
const DEFAULT_PAGESIZE = "100";
exports.DEFAULT_PAGESIZE = DEFAULT_PAGESIZE;
/**
 * Max page size while fetching data.
 */
const MAX_PAGESIZE = 1000;
exports.MAX_PAGESIZE = MAX_PAGESIZE;
/**
 * Basic search operators used by supported models:
 *
 *  - `bw`: begins with
 *  - `eq`: equals
 *  - `ew`: ends with
 *  - `gt`: greater than, inclusive
 *  - `has`: has partial
 *  - `in`: in `string[]`, uses "eq" if only 1 item
 *  - `like`: has partial
 *  - `lt`: lesser than, inclusive
 *  - `neq`: not equals
 *  - `not`: not in `string[]`, uses "neq" if only 1 item
 */
const SEARCH_OPERATORS = {
    bw: "LIKE",
    eq: "=",
    ew: "LIKE",
    gt: ">=",
    has: "LIKE",
    in: "IN",
    like: "LIKE",
    lt: "<=",
    neq: "!=",
    not: "NOT IN",
};
exports.SEARCH_OPERATORS = SEARCH_OPERATORS;
//# sourceMappingURL=constants.js.map