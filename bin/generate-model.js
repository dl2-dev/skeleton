#!/usr/bin/env node

try {
  require("../generator");
} catch (e) {
  require("../src/generator");
}
